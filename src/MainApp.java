import childs.*;
import parents.Shape;

public class MainApp {
	public static void main(String[] args) {
		Shape lingkaran = new Circle("Biru", 20);
		Shape segitiga = new Triangle(10, 15, "Merah");

		System.out.println("luas lingkaran dengan warna "+ lingkaran.getColor() +" adalah "+lingkaran.getArea());
		System.out.println("luas segitiga dengan warna "+ segitiga.getColor() +" adalah "+segitiga.getArea());
	}
}
